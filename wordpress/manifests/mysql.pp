class wordpress::mysql inherits wordpress {
$dbname = "wordpress"
$dbuser = "wordpressuser"
$dbpassword = "password"
$dbhost = "localhost"
mysql::db { $dbname:
  user     => $dbuser,
  password => $dbpassword,
  host     => $dbhost,
  grant    => ['ALL'],
}


}



