class wordpress {
    include wordpress::install
    include wordpress::rootuser
    include wordpress::mysql
     $dbname = $wordpress::mysql::dbname
    $dbuser = $wordpress::mysql::dbuser
    $dbpassword =$wordpress::mysql::dbpassword
    $dbhost =$wordpress::mysql::dbhost
    include wordpress::wp

}

