class wordpress::install inherits wordpress{
exec { 'apt-get-update':
command=>"apt update && touch /var/wp-apt-updated",
path=>"/usr/bin",
creates => "/var/wp-apt-updated",
}

package {"install apache2":
  name => "apache2",
  ensure =>present,
  require => Exec['apt-get-update'],
}


$packages = ['php', 'libapache2-mod-php', 'php-mcrypt','php-mysql']

$packages.each |String $individualpackage| {
  package {"${individualpackage}":
    ensure =>present,
    require => Exec['apt-get-update'],
  }
}

}
